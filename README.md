# README #

A demo app that does not need any dependencies.

### Running the app locally ###

```shell
cd client
yarn
yarn start
```

### Building the docker image ###

```shell
# create a production build of the ui
cd client 
yarn build
```

```shell
# from the project root
docker build -t argo-demo-app-0.0.1 .
```

```shell
# run the docker image
docker run --name argo-demo -p 3009:8080 argo-demo-app-0.0.1
```

Open the browser and navigate to [http://localhost:3009](http://localhost:3009)
