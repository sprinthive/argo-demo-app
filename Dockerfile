FROM mhart/alpine-node:base-8

WORKDIR /app

# Copy the react app
COPY client/build client/build

# Copy all the server dependencies
COPY server/node_modules node_modules

# Copy the whole server
COPY server/src server

EXPOSE 8080
ARG SOURCE_VERSION=unspecified
ENV SOURCE_VERSION $SOURCE_VERSION
CMD NODE_ENV=production DEBUG=sprinthive:* PORT=8080 node server/index.js
