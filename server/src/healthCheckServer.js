#!/usr/bin/env node

const express = require("express");
const app = express();
const http = require('http').Server(app);
const port = process.env.HEALTH_CHECK_PORT || 9080;

app.get('/ping', (req, res) => res.send("OK"));

const server = http.listen(port, () => {
  const port = server.address().port;
  console.log(`Health check server listening at http://localhost:${port}`);
});
