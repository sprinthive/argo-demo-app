#!/usr/bin/env node
require('dotenv-flow').config();
require("./httpServer");
require("./healthCheckServer");
