#!/usr/bin/env node

const express = require("express");
const app = express();
const http = require('http').Server(app);
const path = require('path');
const port = process.env.PORT || 3701;

const startServer = () => {
  // Express only serves static assets in production
  const clientPath = path.resolve(__dirname, '../client/build');
  if (process.env.NODE_ENV === 'production') {
    console.log(`Serving up static content form ${clientPath}`);
    app.use('/', express.static(clientPath));
  }

  const server = http.listen(port, () => {
    const port = server.address().port;
    console.log(`Http server listening at http://localhost:${port}`);
  });
};


startServer()
